
package view;

import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import model.Item;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public class QuartiersCamembert implements IQuartiersCamembert{
 
    private List<Arc2D> listArcs;
    private List<Rectangle2D> listSmallLabel;
    private List<Rectangle2D> listBigLabel;
    
    private final int w = 80;
    private final int h = 30;
    private final int wBig = 100;
    private final int hBig = 100;
 
    public QuartiersCamembert() {
        List<Arc2D> listArcs = new ArrayList<>();
        List<Rectangle2D> listSmallLabel= new ArrayList<>();
        List<Rectangle2D> listBigLabel= new ArrayList<>();
    }
    
    
    @Override
    public List<Arc2D> getListArcs() {
        return listArcs;
    }

    @Override
    public List<Rectangle2D> getListSmallLabel() {
        return listSmallLabel;
    }

    @Override
    public List<Rectangle2D> getListBigLabel() {
        return listBigLabel;
    }
   
    
    @Override
     public void buildArcLabel(List<Item> listItem, float total, double c2, double c3, double x, double y){
    
        Rectangle2D.Double smallLabel = null;
        Rectangle2D.Double bigLabel = null;
        
        this.listArcs = new ArrayList<>();
        this.listSmallLabel= new ArrayList<>();
        this.listBigLabel= new ArrayList<>();
        
        double angle_depart = 0;
        double angle_extent;
        double angle_rect;
        
        for(Item item : listItem ){
            angle_extent = (item.getValeur()/total)*360;
            Arc2D.Double arcItem = new Arc2D.Double(x+(c3-c2)/2, y+(c3-c2)/2, c2, c2, angle_depart, angle_extent, Arc2D.PIE);
            listArcs.add(arcItem);
            Arc2D.Double arcBigLabel = new Arc2D.Double(x, y, c3, c3, angle_depart+angle_extent/2, 0, Arc2D.PIE);
            Arc2D.Double arcSmallLabel = new Arc2D.Double(x+(c3-c2)/2, y+(c3-c2)/2, c2, c2, angle_depart+angle_extent/2, 0, Arc2D.PIE);
            Point2D origin = arcSmallLabel.getStartPoint();
            Point2D originBig = arcBigLabel.getStartPoint();
            angle_rect = (angle_extent/2) + angle_depart;
            if(angle_rect >= 0 && angle_rect < 90){
                smallLabel = new Rectangle2D.Double(origin.getX(), origin.getY()-h, w, h);
                bigLabel = new Rectangle2D.Double(originBig.getX(), originBig.getY()-hBig, wBig, hBig);
            }
            else if(angle_rect >= 90 && angle_rect < 180){
                smallLabel = new Rectangle2D.Double(origin.getX()-w, origin.getY()-h, w, h);
                bigLabel = new Rectangle2D.Double(originBig.getX()-wBig, originBig.getY()-hBig, wBig, hBig);
            }
            else if(angle_rect >= 180 && angle_rect < 270){
                smallLabel = new Rectangle2D.Double(origin.getX()-w, origin.getY(), w, h);
                bigLabel = new Rectangle2D.Double(originBig.getX()-wBig, originBig.getY(), wBig, hBig);
            }
            else if(angle_rect >= 270 && angle_rect < 360){
                smallLabel = new Rectangle2D.Double(origin.getX(), origin.getY(), w, h);
                bigLabel = new Rectangle2D.Double(originBig.getX(), originBig.getY(), wBig, hBig);
            }
            listSmallLabel.add(smallLabel);
            listBigLabel.add(bigLabel);
            
            angle_depart += angle_extent;
        }
    }
}

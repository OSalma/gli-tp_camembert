package view;

import controler.Controler;
import java.awt.Graphics;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public interface ICamembert {
    
    int getiSelect();
    void setiSelect(int iSelect);
    void setControler(Controler c);
    void setSelect(boolean select);
    boolean isSelect();
    void buildQuartier();
    void paint(Graphics g);
    //void selectSuivant();
    //void selectPrecedent();
    
}

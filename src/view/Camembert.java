package view;

import controler.Controler;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JComponent;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public class Camembert extends JComponent implements ICamembert, Observer, MouseListener{
    
    private final double x=150;
    private final double y=100;
    private final double c3;
    private final double c2;
    private final double c1;
    private final double c0;
    private final double start=0;
    private final double extent=360;
    
    Controler controler;
    
    QuartiersCamembert quartier;
    
    private boolean select = false;
    private int iSelect = 0;
    
    private List<Polygon> listButton;
    
    public Camembert(double c3, Controler ctrl) {
        this.c3 = c3;
        c2 = 0.80*c3;
        c1 = 0.65*c3;
        c0 = 0.5*c3;
        this.quartier = new QuartiersCamembert();
        this.controler=ctrl;
        this.listButton = new ArrayList<>();
        Polygon precedent = new Polygon();
        precedent.addPoint(30,10);
        precedent.addPoint(10,30);
        precedent.addPoint(30,50);
        this.listButton.add(precedent);
        Polygon suivant = new Polygon();
        suivant.addPoint(80,10);
        suivant.addPoint(100,30);
        suivant.addPoint(80,50);
        this.listButton.add(suivant);
        
        addMouseListener(this);
    }
    
    @Override
    public int getiSelect() {
        return iSelect;
    }

    @Override
    public void setiSelect(int iSelect) {
        this.iSelect = iSelect;
    }
    
    @Override
    public void setControler(Controler controler) {
        this.controler = controler;
    }
    
    @Override
    public void setSelect(boolean select) {
        this.select = select;
    }
    
    @Override
    public boolean isSelect() {
        return select;
    }

    //Crée une part du camembert avec le label assoicié 
    @Override
    public void buildQuartier(){
        this.quartier.buildArcLabel(controler.getData(), controler.getTotal(), this.c2, this.c3, this.x, this.y);
    }
    
    //Dessine le camembert avec les parts
    @Override
    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        
        buildQuartier();
        
        g2.setRenderingHint(
                        RenderingHints.KEY_ANTIALIASING, 
                        RenderingHints.VALUE_ANTIALIAS_ON);
        //Déssine le gros camembert 
        Arc2D.Double arc3 = new Arc2D.Double(x, y, c3, c3, start, extent, Arc2D.PIE);
        g2.setColor(Color.white);
        g2.draw(arc3);
        g2.fill(arc3);
        
        
        List<Arc2D> listArcs = this.quartier.getListArcs();
        List<Rectangle2D> listSmallLabels = this.quartier.getListSmallLabel();
        List<Rectangle2D> listBigLabels = this.quartier.getListBigLabel();
        int rgb = Color.GRAY.getRGB();
        //Déssine le camembert avec des petits parts et petitis labels 
        //si aucune part n'est selectionné sinon déssine le gros label associé
        for(int i = 0; i < listArcs.size(); i++){
            g2.setColor(new Color(rgb));
            g2.draw(listArcs.get(i));
            g2.fill(listArcs.get(i));
            if(!this.select){
                g2.draw(listSmallLabels.get(i));
                g2.fill(listSmallLabels.get(i));
                g2.setColor(Color.BLACK);
                g2.drawString(""+controler.getData().get(i).getIntitule(),
                        (int) listSmallLabels.get(i).getX()+20,
                        (int) listSmallLabels.get(i).getY()+20);
                
            }
            else if(i == this.iSelect){                
                Arc2D.Double arcSelected = new Arc2D.Double(x, y, c3, c3, listArcs.get(i).getAngleStart(), listArcs.get(i).getAngleExtent(), Arc2D.PIE);
                g2.draw(arcSelected);
                g2.fill(arcSelected);
            }
           
            rgb += 90000;
        }
        //Si une part est selectionnée affiché les polygone suivant et précedent
        //en affichant le gros label et les informations
        if(this.select){
            g2.setColor(Color.RED);
            g2.fill(this.listButton.get(0));            
            g2.fill(this.listButton.get(1));
                                
            g2.setColor(Color.CYAN);
            g2.draw(listBigLabels.get(this.iSelect));
            g2.fill(listBigLabels.get(this.iSelect));
            g2.setColor(Color.BLACK);
            g2.drawString(""+controler.getData().get(this.iSelect).getIntitule(),
                        (int) listBigLabels.get(this.iSelect).getX()+20,
                        (int) listBigLabels.get(this.iSelect).getY()+20);
            g2.setColor(Color.BLACK);
            g2.drawString(""+controler.getData().get(this.iSelect).getDescrption(),
                        (int) listBigLabels.get(this.iSelect).getX()+20,
                        (int) listBigLabels.get(this.iSelect).getY()+40);
            g2.setColor(Color.BLACK);
            g2.drawString(""+controler.getData().get(this.iSelect).getValeur(),
                        (int) listBigLabels.get(this.iSelect).getX()+20,
                        (int) listBigLabels.get(this.iSelect).getY()+70);
        }

        Arc2D.Double arc1 = new Arc2D.Double(x+(c3-c1)/2, y+(c3-c1)/2, c1, c1, start, extent, Arc2D.PIE);
        g2.setColor(Color.white);
        g2.draw(arc1);
        g2.fill(arc1);

        //déssine l'arc ou se trouve le budget total
        Arc2D.Double arc0 = new Arc2D.Double(x+(c3-c0)/2, y+(c3-c0)/2, c0, c0, start, extent, Arc2D.PIE);
        g2.setColor(Color.BLUE);
        g2.draw(arc0);
        g2.fill(arc0);
        g2.setColor(Color.white);
        g2.drawString(""+controler.getTitre(), 280, 280);
        g2.drawString(""+controler.getTotal(), 280, 300);
    }

    //redissiner le camembert a chaque chagement
    @Override
    public void update(Observable o, Object arg) {
        this.repaint();
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        boolean hit = false;
        
        List<Arc2D> listArcs = this.quartier.getListArcs();

        for(int i = 0; i < listArcs.size(); i++){
            if(listArcs.get(i).contains(x,y)){
                this.iSelect = i;
                this.select = true;
                hit = true;
            }
        }
        
        if(this.select){
            if(listButton.get(0).contains(x,y)){
                this.iSelect = (this.iSelect+1)%listArcs.size();
                this.select = true;
                hit = true;
            }
            if(listButton.get(1).contains(x,y)){
                this.iSelect = this.iSelect-1;
                if(this.iSelect < 0)this.iSelect = listArcs.size()-1;
                this.select = true;
                hit = true;
            }
        }
        
        
        if(!hit){
            this.iSelect = 0;
            this.select = false;
        }
        
        

        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
}
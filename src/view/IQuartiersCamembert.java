package view;

import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import model.Item;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public interface IQuartiersCamembert {
    
    List<Arc2D> getListArcs();
    List<Rectangle2D> getListSmallLabel();
    List<Rectangle2D> getListBigLabel();
    void buildArcLabel(List<Item> listItem, float total, 
            double c2, double c3, double x, double y);
    
}

import controler.Controler;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import model.Adapter;
import model.Item;
import view.Camembert;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public class Camembert_interactif {
    
    public static void main(String[] a) {
        
        Adapter adapter = new Adapter();
        Controler controler = new Controler(adapter);
        Camembert camembert = new Camembert(400, controler);
        adapter.addObserver(camembert);
        
        adapter.setTitre("Budget");
        adapter.addItem(new Item("Salma", "GLI", (float) 20.0)); 
        adapter.addItem(new Item("Rida", "GLI", (float) 50.0));
        adapter.addItem(new Item("Seb", "Cadeau Anniversaire", (float) 60.0));        
        adapter.addItem(new Item("Cyp", "Empereur", (float) 80.0));        
        
        JFrame window = new JFrame();
        window.setTitle("TP1 Ouhammouch Salma et Assal Mohamed Rida");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setBounds(0, 0, 800, 700);
        window.setLayout(new BorderLayout());
        window.getContentPane().setBackground(Color.WHITE);
        window.getContentPane().add(camembert);
        window.setVisible(true);
        
    }
}

package controler;

import java.util.List;
import model.Item;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public interface IControler {
    
    String getTitre();
    List<Item> getData();
    float getTotal();
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.util.List;
import model.Adapter;
import model.Item;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public class Controler implements IControler{
    
    Adapter adapter;

    public Controler(Adapter adapter) {
        this.adapter = adapter;
    }
    
    @Override
    public String getTitre(){
        return adapter.getTitre();
    }

    @Override
    public List<Item> getData() {
        return adapter.getItems();
    }

    @Override
    public float getTotal() {
        return adapter.getSommeValue();
    }
    
}

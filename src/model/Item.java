package model;

import java.util.*;

/**
 * @author Ouhammouch Salma & Assal Med Rida
 *
 */
public class Item extends Observable implements IItem{

	private String intitule;
	private String description;
	private float valeur;
	
	public Item(){
		this.intitule = null;
		this.description = null;
		this.valeur = 0;
	}
	
	public Item(String intitule, String description, float valeur){
		this.intitule = intitule;
		this.description = description;
		this.valeur = valeur;
	}
	
	@Override
	public String getIntitule() {
		return this.intitule;
	}

	@Override
	public void setIntitule(String intitule) {
		this.intitule = intitule;
                setChanged();
                notifyObservers();
	}

	@Override
	public String getDescrption() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
                setChanged();
                notifyObservers();
	}

	@Override
	public float getValeur() {
		return this.valeur;
	}

	@Override
	public void setValeur(float valeur) {
		this.valeur = valeur;
                setChanged();
                notifyObservers();
	}
}

package model;

/**
 * @author Ouhammouch Salma & Assal Med Rida
 *
 */
public interface IItem {

	String getIntitule();
	void setIntitule(String intitule);
	
	String getDescrption();
	void setDescription(String description);
	
	float getValeur();
	void setValeur(float valeur);
}

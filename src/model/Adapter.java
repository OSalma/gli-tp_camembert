package model;

import java.util.List;
import java.util.Observable;

/**
 *
 * @author Ouhammouch Salma & Assal Med Rida
 */
public class Adapter extends Observable implements IModel{

    Model m = new Model();

    public Adapter() {}
    
    @Override
    public void setTitre(String titre) {
        m.setTitre(titre);
        setChanged();
        notifyObservers();
    }


    @Override
    public void addItem(Item item) {
        m.addItem(item);
        setChanged();
        notifyObservers();
    }

    @Override
    public void removeItem(Item item) {
        m.removeItem(item);
        setChanged();
        notifyObservers();
    }

   
    @Override
    public float getSommeValue(){
        return m.getSommeValue();
    }

    @Override
    public String getTitre() {
        return m.getTitre();
    }

    @Override
    public List<Item> getItems() {
        return m.getItems();
    }

}
package model;

/**
 * @author Ouhammouch Salma & Assal Med Rida
 *
 */
import java.util.*;

public class Model implements IModel {

	private String titre;
	private List<Item> items;
        private float total;
	
	public Model(){
            this.titre="MyModel";
            this.items = new ArrayList<>();
            this.total = 0;
	}
	
	@Override
	public String getTitre() {
		return this.titre;
	}

	@Override
	public void setTitre(String titre) {
		this.titre = titre;
                
	}

	@Override
	public List<Item> getItems() {
		return new ArrayList<>(this.items);
	}

	@Override
	public void addItem(Item item) {
		this.items.add(item);
                this.total += item.getValeur();
                
	}

	@Override
	public void removeItem(Item item) {
            this.items.remove(item);
            this.total -= item.getValeur();
                
	}

    @Override
    public float getSommeValue() {
        return this.total;
    }
  
}

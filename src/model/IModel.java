package model;

/**
 * @author Mohamed Rida Assal & Ouhammouch Salma
 *
 */
import java.util.*;

public interface IModel {

	String getTitre();
	void setTitre(String titre);
	
 	List<Item> getItems();
	void addItem(Item item);
	void removeItem(Item item);
        
        float getSommeValue();
}
